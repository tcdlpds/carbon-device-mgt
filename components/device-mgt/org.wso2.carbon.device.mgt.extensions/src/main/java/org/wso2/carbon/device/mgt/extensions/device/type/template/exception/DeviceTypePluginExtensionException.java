package org.wso2.carbon.device.mgt.extensions.device.type.template.exception;

public class DeviceTypePluginExtensionException extends Exception {

    public DeviceTypePluginExtensionException(String msg) {
        super(msg);
    }

    public DeviceTypePluginExtensionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
